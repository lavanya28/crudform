package com.example.CrudForm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;

@SpringBootApplication(exclude = {UserDetailsServiceAutoConfiguration.class})
public class CrudFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudFormApplication.class, args);
	}
//https://suraj_store.bikry.com/
	//business entitycontroller,booking controller
}
