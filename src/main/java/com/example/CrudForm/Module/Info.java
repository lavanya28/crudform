package com.example.CrudForm.Module;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Getter
@Setter

@Entity(name="student")
public @Data
class Info implements Serializable {
    private static final long serialVersionUID = -2346866669992342L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "rollno")
    private int rollno;

    @Column(name = "phnno")
    private String phnno;

    @Column(name = "state")
    private String state;
    private String image;


    public void setname(String name) {
        this.name = name;
    }
    public String getname() {
        return name;
    }


    public int getrollno() {
        return rollno;
    }
    public void setrollno(int rollno) {
        this.rollno = rollno;
    }

    public String getphnno() {
        return phnno;
    }
    public void setphnno(String phnno) {
        this.phnno = phnno;
    }

    public String getstate() {
        return state;
    }
    public void setstate(String state) {this.state = state; }
    public String getimage() {
        return image;
    }
    public void setimage(String image) {
        this.image = image;
    }





}



