package com.example.CrudForm.Service;


import com.example.CrudForm.Module.Info;
import com.example.CrudForm.Repository.DataRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class DataService {
@Autowired
DataRepository dataRepository;


    public List<Info> findAll() {
        List<Info> bes = (List<Info>) dataRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        return bes;
    }
    public Info adddata(Info info)  {

        Info result= this.dataRepository.save(info);
        return result;

    }
    public Info updateRecord(Info areaInfo,long id) {
       Info result= dataRepository.save(areaInfo);
       return result;
    }
    public void deleteById(long id) {
        dataRepository.deleteById(id);
    }


    public Optional<Info> findById(long id) {
        Optional<Info> inf=dataRepository.findById(id);
        return inf;
    }

    public Info saveUser(String name, int rollno, String phnno, String state,String image) throws Exception {
Info inf=new Info();
        if(phnno.length()<10 || phnno.length()>10)
        {
            throw new Exception("Invalid phnno");
        }
        else
        {
           inf.setphnno(phnno);
        }
inf.setname(name);
inf.setrollno(rollno);
inf.setstate(state);
inf.setimage(image);
dataRepository.save(inf);
return inf;
    }
}
