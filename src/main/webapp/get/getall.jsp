<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<link rel="../stylesheet/style.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script
                    src="https://code.jquery.com/jquery-3.6.0.min.js"
                    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                    crossorigin="anonymous"
            ></script>
            <script
                    src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
                    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
                    crossorigin="anonymous"
            ></script>
            <script
                    src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
                    integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
                    crossorigin="anonymous"
            ></script>
            <link
                    rel="stylesheet"
                    href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
                    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
                    crossorigin="anonymous"
            />

            <link
                    rel="stylesheet"
                    href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
                    integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
                    crossorigin="anonymous"
            />
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" href="/html/tas"><h1>GetData</h1> <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/html/user"><h1>Add</h1></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/html/tas"><h1>Update</h1></a>
              </li>

            </ul>
          </div>
        </nav>
<div id="demo" class="container" >
    <table class="table table-striped mt-5">
        <tr>
            <th>ID</th>
            <th>NAME</th>
            <th>ROLLNO</th>
            <th>PHNNO</th>
            <th>STATE</th>
            <th>EDIT</th>
            <th>DELETE</th>
        </tr>
        <c:forEach var="task" items="${tasks}">

            <tr>
                <td>${task.id}</td>
                <td>${task.name}</td>
                <td>${task.rollno}</td>
                <td>${task.phnno}</td>
                <td>${task.state}</td>
                <td><a href="/html/formredirect/${task.id}">EDIT</a></td>
                <td><a href="/html/delete/${task.id}">DELETE</a></td>



            </tr>
        </c:forEach>

    </table></div>
</body>
</html>