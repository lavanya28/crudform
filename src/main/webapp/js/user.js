function saveUser() {


    var name = $("#name").val();
    var rollno = $("#rollno").val();
    var phnno = $("#phnno").val();
    var state = $("#state").val();
    var image= $("#image").val();
var html;
    $.ajax({
        type: "POST",
        url: "/ajax/saveUser",
        dataType: "json",
        data: {
            name: name,
            rollno: rollno,
            phnno: phnno,
            state: state,
            image:image,
        },
        success: function(response) {
            console.log(response);

                     alert("Inserted Successfully!!");
                 
}
    });
    return false;
}


function getAllUsers() {

console.log("hey");
     var html;
    $.ajax({
        type: "GET",
        url: "/ajax/getall",

        success: function(result) {
        console.log(result);
             html="";
             html+="<div  class='container mt-5'>";
             html+=" <div class='row'>";
             for (var i=0;i<result.length;i++)
              {

               var row = result[i];
               var id=row.id;
               var name = row.name;
               var rollno = row.rollno;
               var  phnno= row.phnno;
               var state = row.state;
               var image=row.image;

               html+="<div class='card p-3 mb-3 ml-3'>";

               html+="<div class='card' style='width: 18rem; margin-bottom:30px; box-shadow:  0 2px 6px 0 #1a181e66'>";
               html+="<img class='card-img-top' src='"+image+"' alt='Card image cap' style='height:180 ; width:286'>";
               html+="<div class='card-body'>";
               html+="<div class='d-flex justify-content-end'>";
               html+="<a class='card-link'  href='/ajax/redirectajax/"+id+"'><i class='fa fa-pencil-square-o fa-lg' aria-hidden='true'></i></a>"+
                      "<a  class='card-link'  href='/ajax/del/"+id+"'><i class='fa fa-trash-o fa-lg' aria-hidden='true'></i></a>";
               html+="</div>";
               html+= "<h5 class='card-title'>" +id + "</h5>" +
                       "<h6 class='card-subtitle mb-2'>" +name + "</h6>" +
                       "<h6 class='card-subtitle mb-2'>" +rollno + "</h6>" +
                       "<h6 class='card-subtitle mb-2'> <i class='fa fa-phone' aria-hidden='true'></i><span>  </span>"+phnno + "</h6>" +
                       "<h6 class='card-subtitle mb-2'>" + state + "</h6>" ;
                              html+= "</div>";
                              html+= "</div>";
                              html+="</div>";


             }
             html+="</div>";
             html+="</div>";


           $("#show").html(html);
        },
        error: function(response) {
            console.log(response);
        }

    });
    }



              function editUser() {

                  var id = $("#id").val();
                  var name = $("#name").val();
                  var rollno = $("#rollno").val();
                  var phnno = $("#phnno").val();
                  var state = $("#state").val();
                  var image = $("#image").val();
                  $.ajax({
                      type: "POST",
                      url: "/ajax/editUser",
                      dataType: "json",
                      data: {
                          id:id,
                          name: name,
                          rollno: rollno,
                          phnno: phnno,
                          state: state,
                          image:image
                      },
                      success: function(response) {
                          console.log(response);

                                   alert("updated Successfully!!");

              },
              error: function(response) {
                          console.log(response);
                      }
                  });
                  return false;
              }

